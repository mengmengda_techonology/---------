## 确认github for win版本 ##
![img](https://bytebucket.org/mengmengda_techonology/---------/raw/6a92cc31ad6d3206b302fb2f6ba0bb92028cf146/SilverLightR/github%20for%20win%E4%BB%8Ebitbucket%20clone%E9%A1%B9%E7%9B%AE%5B2015-12-25%5D/images/1.png)

## 1. 获取项目链接 ##
访问以下网址，拖动图片中红框位置的链接，到github for win中

[mengmengda_techonology / 萌萌达技术分享目录 — Bitbucket](https://bitbucket.org/mengmengda_techonology/---------)

![img](https://bytebucket.org/mengmengda_techonology/---------/raw/6a92cc31ad6d3206b302fb2f6ba0bb92028cf146/SilverLightR/github%20for%20win%E4%BB%8Ebitbucket%20clone%E9%A1%B9%E7%9B%AE%5B2015-12-25%5D/images/2.png)
![img](https://bytebucket.org/mengmengda_techonology/---------/raw/6a92cc31ad6d3206b302fb2f6ba0bb92028cf146/SilverLightR/github%20for%20win%E4%BB%8Ebitbucket%20clone%E9%A1%B9%E7%9B%AE%5B2015-12-25%5D/images/1.gif)

## 2. 设置项目repo的自己的user.email ##
用Git Shell切换到项目根目录，用以下命令设置user信息

`
git config  user.email "xxxx@xx.com"

git config  user.name "suzie"
`

user.email：bitbucket注册邮箱

user.name：bitbucket用户名

![img](https://bytebucket.org/mengmengda_techonology/---------/raw/6a92cc31ad6d3206b302fb2f6ba0bb92028cf146/SilverLightR/github%20for%20win%E4%BB%8Ebitbucket%20clone%E9%A1%B9%E7%9B%AE%5B2015-12-25%5D/images/3.png)

## 3. 测试上传文件，到本地git ##
项目根目录创建文件，然后随便写点文字

切换到github for win，如下图填写提交文本，然后点击comment提交（这里是提交到本地git）

![img](https://bytebucket.org/mengmengda_techonology/---------/raw/6a92cc31ad6d3206b302fb2f6ba0bb92028cf146/SilverLightR/github%20for%20win%E4%BB%8Ebitbucket%20clone%E9%A1%B9%E7%9B%AE%5B2015-12-25%5D/images/4.png)

## 4. 将文件从本地git，上传到网络项目中 ##
步骤3提交成功的话，会如下图有个created comment的信息

右上角的Sync有个1的标示，表示有一个文件被改变，点击Sync上传变更到网络项目中

![img](https://bytebucket.org/mengmengda_techonology/---------/raw/6a92cc31ad6d3206b302fb2f6ba0bb92028cf146/SilverLightR/github%20for%20win%E4%BB%8Ebitbucket%20clone%E9%A1%B9%E7%9B%AE%5B2015-12-25%5D/images/5.png)

## 5. 查看变更是否成功上传 ##
访问以下网址，看看右侧的活动信息是否有刚才提交的文件

[mengmengda_techonology / 萌萌达技术分享目录 — Bitbucket](https://bitbucket.org/mengmengda_techonology/---------)

如下图，哥是上传成功啦

![img](https://bytebucket.org/mengmengda_techonology/---------/raw/6a92cc31ad6d3206b302fb2f6ba0bb92028cf146/SilverLightR/github%20for%20win%E4%BB%8Ebitbucket%20clone%E9%A1%B9%E7%9B%AE%5B2015-12-25%5D/images/6.png)